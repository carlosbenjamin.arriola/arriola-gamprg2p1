// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAMPRG2P1GameMode.generated.h"

UCLASS(minimalapi)
class AGAMPRG2P1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAMPRG2P1GameMode();
};



