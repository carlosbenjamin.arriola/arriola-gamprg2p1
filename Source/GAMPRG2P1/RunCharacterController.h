// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class GAMPRG2P1_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

	class ARunCharacter* Character;

	void MoveForward(float scale);
	void MoveRight(float scale);

	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	
public:

	
};

