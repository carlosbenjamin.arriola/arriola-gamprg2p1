// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GAMPRG2P1 : ModuleRules
{
	public GAMPRG2P1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
