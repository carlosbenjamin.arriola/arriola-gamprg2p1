// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Templates/SubclassOf.h"
#include "Pickup.h"
#include "Obstacle.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Tile = CreateDefaultSubobject<USceneComponent>(TEXT("Tile"));
	SetRootComponent(Tile);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->SetupAttachment(Tile);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ExitTrigger"));
	ExitTrigger->SetupAttachment(Tile);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnOverlapBegin);

	ObstacleSpawnArea = CreateDefaultSubobject<UBoxComponent>(TEXT("ObstacleSpawn"));
	ObstacleSpawnArea->SetupAttachment(Tile);

	PickupSpawnArea = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupSpawn"));
	PickupSpawnArea->SetupAttachment(Tile);

	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	Floor->SetupAttachment(Tile);

	Wall1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall1"));
	Wall1->SetupAttachment(Tile);

	Wall2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall2"));
	Wall2->SetupAttachment(Tile);

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	SpawnObstacles();
	SpawnPickups();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ATile::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* RunCharacter = Cast<ARunCharacter>(OtherActor))
	{
		Exited.Broadcast(this);
		UE_LOG(LogTemp, Warning, TEXT("Character passed"));
	}
}

void ATile::SpawnObstacles()
{
	ObstacleSpawnPoint = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnArea->GetComponentLocation(), ObstacleSpawnArea->GetScaledBoxExtent());
	AObstacle* SpawnedObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleType, ObstacleSpawnPoint, FRotator(0), FActorSpawnParameters());
	SpawnedObstacle->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true), ATile::GetAttachParentSocketName());
	//ObjectsToDestroy.Add(SpawnedObstacle);
	OnDestroyed.AddDynamic(SpawnedObstacle, &AObstacle::DestroyActor);
}

void ATile::SpawnPickups()
{
	PickupSpawnPoint = UKismetMathLibrary::RandomPointInBoundingBox(PickupSpawnArea->GetComponentLocation(), PickupSpawnArea->GetScaledBoxExtent());
	APickup* SpawnedPickup = GetWorld()->SpawnActor<APickup>(PickupType, PickupSpawnPoint, FRotator(0), FActorSpawnParameters());
	SpawnedPickup->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true), ATile::GetAttachParentSocketName());
	OnDestroyed.AddDynamic(SpawnedPickup, &APickup::DestroyActor);
}

//void ATile::Destroyed()
//{
//	Super::Destroyed();
//
//	for (AActor * actor : ObjectsToDestroy)
//	{
//		if (actor)
//		{
//			actor->Destroy();
//		}
//	}
//}




