// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GAMPRG2P1_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ARunGameMode();

	UPROPERTY(EditAnywhere, Category = "Config")
		TSubclassOf<ATile> BP_Tile;

	UPROPERTY(EditAnywhere)
		int32 InitialFloorTiles = 10;

	UPROPERTY(VisibleInstanceOnly, Category = "Runtime")
		FTransform NextSpawnPosition;

	UFUNCTION()
		void AddFloorTile();

	UFUNCTION()
		void SpawnInitialFloorTiles();

	UFUNCTION()
		void Exited(class ATile* Tile);

	UFUNCTION()
		void DestroyTile(class ATile* Tile);

protected:

	virtual void BeginPlay() override;


};
