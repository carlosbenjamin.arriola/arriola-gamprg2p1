// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "RunCharacter.h"
#include "Tile.h"

ARunGameMode::ARunGameMode()
{
	
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	SpawnInitialFloorTiles();
}

void ARunGameMode::SpawnInitialFloorTiles()
{
	for (int i = 0; i < InitialFloorTiles; i++)
	{
		AddFloorTile();
	}
}

void ARunGameMode::Exited(ATile* Tile)
{
	AddFloorTile();
	
	FTimerHandle timerHandle;

	FTimerDelegate timerDelegate = FTimerDelegate::CreateUObject(this, &ARunGameMode::DestroyTile, Tile);

	GetWorldTimerManager().SetTimer(timerHandle, timerDelegate, 1.0f, false);
}

void ARunGameMode::AddFloorTile()
{
	ATile* Tile = GetWorld()->SpawnActor<ATile>(BP_Tile, NextSpawnPosition);
	UE_LOG(LogTemp, Warning, TEXT("Tile spawned"));
	if (Tile)
	{
		Tile->Exited.AddDynamic(this, &ARunGameMode::Exited);
		NextSpawnPosition = Tile->GetAttachTransform();
	}
}

void ARunGameMode::DestroyTile(ATile* Tile)
{
	Tile->Destroy();
}
