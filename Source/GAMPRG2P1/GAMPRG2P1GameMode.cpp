// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAMPRG2P1GameMode.h"
#include "GAMPRG2P1Character.h"
#include "UObject/ConstructorHelpers.h"

AGAMPRG2P1GameMode::AGAMPRG2P1GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
