// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	Character = Cast<ARunCharacter>(ARunCharacterController::GetPawn());
}

void ARunCharacterController::MoveForward(float scale)
{
	Character->AddMovementInput(Character->GetActorForwardVector(), scale);
}

void ARunCharacterController::MoveRight(float scale)
{
	Character->AddMovementInput(Character->GetActorRightVector(), scale);
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Character->PlayerAlive())
	{
		MoveForward(1);
	}
	else
		MoveForward(0);
	
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}


