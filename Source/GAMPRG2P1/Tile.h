// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Tile.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTileExited, class ATile*, Tile);

class UStaticMeshComponent;

UCLASS()
class GAMPRG2P1_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	FORCEINLINE const FTransform& GetAttachTransform() const
	{
		return AttachPoint->GetComponentTransform();
	}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	//virtual void Destroyed() override;


public:	
	// Called every frame

	UPROPERTY(EditAnywhere, Category = "Config")
		TSubclassOf<class AObstacle> ObstacleType;

	UPROPERTY(EditAnywhere, Category = "Config")
		TSubclassOf<class APickup> PickupType;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* Tile;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* ObstacleSpawnArea;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* PickupSpawnArea;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Floor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Wall1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Wall2;

	UPROPERTY(BlueprintAssignable)
		FOnTileExited Exited;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector ObstacleSpawnPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector PickupSpawnPoint;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void SpawnObstacles();

	UFUNCTION()
		void SpawnPickups();


private:

	//TArray<AActor*> ObjectsToDestroy;

};
